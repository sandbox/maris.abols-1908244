<?php

/**
 * @file
 * First Data LV redirected payment service SMS processing functions.
 */

/**
 * SMS payment process function.
 */
function commerce_ibis_process_sms() {
  $settings = commerce_ibis_get_rule_settings();

  $ecomm_client_url = ($settings['commerce_ibis_server']['commerce_ibis_server'] == 'test') ? $settings['commerce_ibis_server']['client_url_test'] : $settings['commerce_ibis_server']['client_url_live'];
  $ecomm_server_url = ($settings['commerce_ibis_server']['commerce_ibis_server'] == 'test') ? $settings['commerce_ibis_server']['server_url_test'] : $settings['commerce_ibis_server']['server_url_live'];

  $amount = $_REQUEST['total'] * 100;
  $ip = ip_address();
  // Use localhost IP address to make testing procedure for blakclist IP.
  // $ip = '192.168.1.2'.
  $currency = $_REQUEST['currency'];
  $description = urlencode(htmlspecialchars($_REQUEST['description'], ENT_QUOTES));
  $language = $_REQUEST['lang'];
  $ibis_order_id = $_REQUEST['cart_order_id'];
  $now = REQUEST_TIME;

  $merchant = new Merchant($ecomm_server_url, $settings['commerce_ibis_server']['commerce_ibis_cert_path'], $settings['commerce_ibis_server']['commerce_ibis_cert_pass'], 1);

  $resp = $merchant->startSMSTrans($amount, $currency, $ip, $description, $language);

  if (substr($resp, 0, 14) == "TRANSACTION_ID") {
    $trans_id = substr($resp, 16, 28);
    $url = $ecomm_client_url . '?trans_id=' . urlencode($trans_id) . '&ibis_order_id=' . $ibis_order_id;
    $result = db_insert('commerce_ibis_transaction')
    ->fields(array(
      'trans_id' => $trans_id,
      'amount' => $amount,
      'currency' => $currency,
      'client_ip_addr' => $ip,
      'order_id' => $ibis_order_id,
      'description' => $_REQUEST['description'],
      'language' => $language,
      't_date' => $now,
      'response' => $resp,
    ))
    ->execute();

    drupal_goto($url);
  }
  else {
    watchdog('ibis', 'SMS payment make failed: !result', array('!result' => $resp));
    $result = db_insert('commerce_ibis_error')
    ->fields(array(
      'error_time' => $now,
      'action' => 'startSMStrans',
      'response' => $resp,
    ))
    ->execute();
    drupal_set_message(check_plain(t('Technical error occurred! Please contact merchant!')));
    drupal_goto();
  }
}
