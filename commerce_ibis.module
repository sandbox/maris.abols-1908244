<?php

/**
 * @file
 * Integrates First Data redirected payment service.
 */

/**
 * Ecomm server urls
 */

define('COMMERCE_IBIS_RULE_NAME', 'commerce_payment_ibis');


/**
 * Implements hook_init().
 */
function commerce_ibis_init() {
  $merchant_path = NULL;
  if (module_exists('libraries')) {
    // Support Libraries API - http://drupal.org/project/libraries
    $merchant_path = libraries_get_path('Merchant');
  }

  // Include Merchant.php if it exists.
  if (!empty($merchant_path)) {
    include_once $merchant_path . '/Merchant.php';
  }
}

/**
 * Implements hook_menu().
 */
function commerce_ibis_menu() {
  $items = array();

  $items['cart/ibis/complete'] = array(
    'title' => 'Order complete',
    'page callback' => 'commerce_ibis_complete',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'commerce_ibis.pages.inc',
  );
  $items['cart/ibis/failed'] = array(
    'title' => 'Order failed',
    'page callback' => 'commerce_ibis_failed',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'commerce_ibis.pages.inc',
  );
  $items['cart/ibis/process/sms'] = array(
    'title' => 'Process IBIS SMS payment',
    'page callback' => 'commerce_ibis_process_sms',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'commerce_ibis.sms.inc',
  );
  $items['cart/ibis/process/dms'] = array(
    'title' => 'Process IBIS DMS payment',
    'page callback' => 'commerce_ibis_process_dms',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'commerce_ibis.dms.inc',
  );
  $items['cart/ibis/make/dms'] = array(
    'title' => 'Process IBIS DMS payment',
    'page callback' => 'commerce_ibis_make_dms',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'commerce_ibis.dms.inc',
  );
  $items['cart/ibis/reverse'] = array(
    'title' => 'Reverse payment',
    'page callback' => 'commerce_ibis_reverse',
    'access arguments' => array('manual payments'),
    'type' => MENU_CALLBACK,
    'file' => 'commerce_ibis.pages.inc',
  );
  $items['admin/store/orders/%uc_order/payments/reverse'] = array(
    'title' => 'Reverse payment',
    'description' => 'Reverse payment',
    'page callback' => 'commerce_ibis_reverse_page',
    'access arguments' => array('manual payments'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'commerce_ibis.pages.inc',
  );

  $items['admin/store/orders/%uc_order/payments/dms'] = array(
    'title' => 'Make IBIS DMS payment',
    'description' => 'Make IBIS DMS payment',
    'page callback' => 'commerce_ibis_make_dms_page',
    'access arguments' => array('manual payments'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'commerce_ibis.dms.inc',
  );

  return $items;
}

/**
 * Implements hook_commerce_payment_method_info().
 */
function commerce_ibis_commerce_payment_method_info() {
  $payment_methods = array();

  $path = base_path() . drupal_get_path('module', 'commerce_ibis');
  $title = variable_get('commerce_ibis_method_title', 'FirstData CC');

  $payment_methods['commerce_ibis'] = array(
    'title' => $title,
    'display_title' => t('Credit card IBIS'),
    'short_title' => 'ibis',
    'description' => t('Redirect to FirstData to pay by credit card.'),
    'terminal' => FALSE,
    'offsite' => TRUE,
    'offsite_autoredirect' => TRUE,
  );

  return $payment_methods;
}

/**
 * Function to load rule settings manually.
 */
function commerce_ibis_get_rule_settings() {
  $settings = array();

  $rule = rules_config_load(COMMERCE_IBIS_RULE_NAME);
  foreach ($rule->actions() as $action) {
    $settings[] = $action->settings;
  }

  if (!empty($settings) && !empty($settings[0]['payment_method'])) {
    return $settings[0]['payment_method']['settings'];
  }

  return NULL;
}

/**
 * Payment method callback: settings form.
 */
function commerce_ibis_settings_form($settings = NULL) {
  $form = array();

  $form['commerce_ibis_method_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Payment method title'),
    '#description' => t('Provide specific description for the payment method on the order checkout page.'),
    '#default_value' => (isset($settings['commerce_ibis_method_title']) && $settings['commerce_ibis_method_title'] ? $settings['commerce_ibis_method_title'] : 'Credit card on a secure FirstData server'),
  );

  $form['commerce_ibis_policy'] = array(
    '#type' => 'textarea',
    '#title' => t('Payment instructions'),
    '#description' => t('Instructions for customers on the checkout page. Use &lt;br /&gt; for line break.'),
    '#default_value' => (isset($settings['commerce_ibis_policy']) && $settings['commerce_ibis_policy'] ? $settings['commerce_ibis_policy'] : ''),
    '#rows' => 3,
  );

  $form['commerce_ibis_checkout_button'] = array(
    '#type' => 'textfield',
    '#title' => t('Order submit button text'),
    '#description' => t('Order submit button text on checkout review page.'),
    '#default_value' => (isset($settings['commerce_ibis_checkout_button']) && $settings['commerce_ibis_checkout_button'] ? $settings['commerce_ibis_checkout_button'] : t('Submit Order')),
  );

  $form['commerce_ibis_server'] = array(
    '#type' => 'fieldset',
    '#title' => t('Server settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['commerce_ibis_server']['commerce_ibis_server'] = array(
    '#type' => 'select',
    '#title' => t('IBIS server'),
    '#options' => array(
      'test' => 'Test',
      'live' => 'Live',
    ),
    '#default_value' => isset($settings['commerce_ibis_server']['commerce_ibis_server']) && $settings['commerce_ibis_server']['commerce_ibis_server'] ? $settings['commerce_ibis_server']['commerce_ibis_server'] : '',
  );

  $form['commerce_ibis_server']['server_url_live'] = array(
    '#type' => 'textfield',
    '#title' => t('Server url live'),
    '#default_value' => isset($settings['commerce_ibis_server']['server_url_live']) && $settings['commerce_ibis_server']['server_url_live'] ? $settings['commerce_ibis_server']['server_url_live'] : '',
    '#description' => t('Url for live server if selected "Live" in "IBIS server"'),
  );

  $form['commerce_ibis_server']['client_url_live'] = array(
    '#type' => 'textfield',
    '#title' => t('Client url live'),
    '#default_value' => isset($settings['commerce_ibis_server']['client_url_live']) && $settings['commerce_ibis_server']['client_url_live'] ? $settings['commerce_ibis_server']['client_url_live'] : '',
    '#description' => t('Url for live client if selected "Live" in "IBIS server"'),
  );

  $form['commerce_ibis_server']['server_url_test'] = array(
    '#type' => 'textfield',
    '#title' => t('Server url test'),
    '#default_value' => isset($settings['commerce_ibis_server']['server_url_test']) && $settings['commerce_ibis_server']['server_url_test'] ? $settings['commerce_ibis_server']['server_url_test'] : '',
    '#description' => t('Url for test server if selected "Test" in "IBIS server"'),
  );

  $form['commerce_ibis_server']['client_url_test'] = array(
    '#type' => 'textfield',
    '#title' => t('Client url test'),
    '#default_value' => isset($settings['commerce_ibis_server']['client_url_test']) && $settings['commerce_ibis_server']['client_url_test'] ? $settings['commerce_ibis_server']['client_url_test'] : '',
    '#description' => t('Url for test client if selected "Test" in "IBIS server"'),
  );

  // 428=LVL 978=EUR 840=USD 941=RSD 703=SKK 440=LTL 233=EEK 643=RUB
  // 891=YUM.
  $form['commerce_ibis_server']['commerce_ibis_currency'] = array(
    '#type' => 'select',
    '#title' => t('Currency'),
    '#options' => array(
      '978' => 'EUR',
      '428' => 'LVL',
      '840' => 'USD',
    ),
    '#default_value' => isset($settings['commerce_ibis_server']['commerce_ibis_currency']) && $settings['commerce_ibis_server']['commerce_ibis_currency'] ? $settings['commerce_ibis_server']['commerce_ibis_currency'] : '',
  );
  $form['commerce_ibis_server']['commerce_ibis_payment_type'] = array(
    '#type' => 'select',
    '#title' => t('Payment type'),
    '#options' => array(
      'sms' => 'SMS',
      'dms' => 'DMS',
    ),
    '#default_value' => isset($settings['commerce_ibis_server']['commerce_ibis_payment_type']) && $settings['commerce_ibis_server']['commerce_ibis_payment_type'] ? $settings['commerce_ibis_server']['commerce_ibis_payment_type'] : '',
  );
  $form['commerce_ibis_server']['commerce_ibis_cert_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Full path to keystore file'),
    '#default_value' => isset($settings['commerce_ibis_server']['commerce_ibis_cert_path']) && $settings['commerce_ibis_server']['commerce_ibis_cert_path'] ? $settings['commerce_ibis_server']['commerce_ibis_cert_path'] : '',
  );
  $form['commerce_ibis_server']['commerce_ibis_cert_pass'] = array(
    '#type' => 'textfield',
    '#title' => t('Keystore password'),
    '#default_value' => isset($settings['commerce_ibis_server']['commerce_ibis_cert_pass']) && $settings['commerce_ibis_server']['commerce_ibis_cert_pass'] ? $settings['commerce_ibis_server']['commerce_ibis_cert_pass'] : '',
  );

  return $form;
}

/**
 * Function that proceed IBIS payment form.
 */
function commerce_ibis_payment_form(&$form, &$form_state, $order, $settings) {
  global $language;

  $total = $order->commerce_order_total['und'][0]['amount'];

  $data = array(
    'sid' => $order->order_id,
    'total' => $total / 100,
    'cart_order_id' => $order->order_id,
    'lang' => $language->language,
    'x_receipt_link_url' => url('cart/ibis/complete/' . $order->order_id, array('absolute' => TRUE)),
    'merchant_order_id' => $order->order_id,
    'id_type' => 1,
    'currency' => $settings['settings']['commerce_ibis_server']['commerce_ibis_currency'],
    'description' => t('Order #!order payment', array('!order' => check_plain($order->order_number))),
  );

  $form['#action'] = base_path() . 'cart/ibis/process/' . $settings['settings']['commerce_ibis_server']['commerce_ibis_payment_type'];

  foreach ($data as $name => $value) {
    $form[$name] = array(
      '#type' => 'hidden',
      '#value' => $value,
    );
  }

  return $form;
}

/**
 * Build payment redirect form.
 */
function commerce_ibis_redirect_form($form, &$form_state, $order, $settings) {
  $form = array();

  $form_state['order'] = $order;
  $form_state['transaction'] = $settings;

  commerce_ibis_payment_form($form, $form_state, $order, $settings);

  return $form;
}

/**
 * Payment method callback: checkout form.
 */
function commerce_ibis_submit_form($payment_method, $pane_values, $checkout_pane, $order) {
  $form = array();
  $form['commerce_ibis_policy'] = array(
    '#markup' => $payment_method['settings']['commerce_ibis_policy'],
  );
  return $form;
}

/**
 * Cron function.
 */
function commerce_ibis_cron() {
  $settings = commerce_ibis_get_rule_settings();

  $ecomm_server_url = ($settings['commerce_ibis_server']['commerce_ibis_server'] == 'test') ? $settings['commerce_ibis_server']['server_url_test'] : $settings['commerce_ibis_server']['server_url_live'];

  if (variable_get("IBIS_lastclosedate", 0) != format_date(REQUEST_TIME, 'custom', 'Ymd')) {
    $merchant = new Merchant($ecomm_server_url, $settings['commerce_ibis_server']['commerce_ibis_cert_path'], $settings['commerce_ibis_server']['commerce_ibis_cert_pass'], 1);
    $resp = $merchant->closeDay();

    watchdog('ibis', 'IBIS close business day: ' . $resp);
    variable_set("IBIS_lastclosedate", format_date(REQUEST_TIME, 'custom', 'Ymd'));

    // RESULT: OK RESULT_CODE: 500 FLD_075: 4 FLD_076: 6 FLD_087: 40.
    // FLD_088: 60.
    if (strstr($resp, 'RESULT:')) {
      $result = explode('RESULT: ', $resp);
      $result = preg_split('/\r\n|\r|\n/', $result[1]);
      $result = $result[0];
    }
    else {
      $result = '';
    }

    if (strstr($resp, 'RESULT_CODE:')) {
      $result_code = explode('RESULT_CODE: ', $resp);
      $result_code = preg_split('/\r\n|\r|\n/', $result_code[1]);
      $result_code = $result_code[0];
    }
    else {
      $result_code = '';
    }

    if (strstr($resp, 'FLD_075:')) {
      $count_reversal = explode('FLD_075: ', $resp);
      $count_reversal = preg_split('/\r\n|\r|\n/', $count_reversal[1]);
      $count_reversal = $count_reversal[0];
    }
    else {
      $count_reversal = '';
    }

    if (strstr($resp, 'FLD_076:')) {
      $count_transaction = explode('FLD_076: ', $resp);
      $count_transaction = preg_split('/\r\n|\r|\n/', $count_transaction[1]);
      $count_transaction = $count_transaction[0];
    }
    else {
      $count_transaction = '';
    }

    if (strstr($resp, 'FLD_087:')) {
      $amount_reversal = explode('FLD_087: ', $resp);
      $amount_reversal = preg_split('/\r\n|\r|\n/', $amount_reversal[1]);
      $amount_reversal = $amount_reversal[0];
    }
    else {
      $amount_reversal = '';
    }

    if (strstr($resp, 'FLD_088:')) {
      $amount_transaction = explode('FLD_088: ', $resp);
      $amount_transaction = preg_split('/\r\n|\r|\n/', $amount_transaction[1]);
      $amount_transaction = $amount_transaction[0];
    }
    else {
      $amount_transaction = '';
    }

    $now = format_date(REQUEST_TIME, 'custom', 'Y-m-d H:i:s');
    $result = db_insert('commerce_ibis_batch')
    ->fields(array(
      'result' => $result,
      'result_code' => $result_code,
      'count_reversal' => $count_reversal,
      'count_transaction' => $count_transaction,
      'amount_reversal' => $amount_reversal,
      'amount_transaction' => $amount_transaction,
      'close_date' => $now,
      'response' => $resp,
    ))
    ->execute();
  }
}
