<?php

/**
 * @file
 * First Data redirected payment service pages.
 */

/**
 * Payment complete function.
 */
function commerce_ibis_complete() {
  $settings = commerce_ibis_get_rule_settings();

  $ecomm_server_url = ($settings['commerce_ibis_server']['commerce_ibis_server'] == 'test') ? $settings['commerce_ibis_server']['server_url_test'] : $settings['commerce_ibis_server']['server_url_live'];

  $order_id = $_REQUEST['ibis_order_id'];
  $trans_id = $_REQUEST['trans_id'];

  $now = REQUEST_TIME;

  $merchant = new Merchant($ecomm_server_url, $settings['commerce_ibis_server']['commerce_ibis_cert_path'], $settings['commerce_ibis_server']['commerce_ibis_cert_pass'], 1);

  $resp = $merchant->getTransResult(urlencode($trans_id), ip_address());

  $result_code = explode(':', $resp);
  $result_code = substr($result_code[2], 1, 3);

  $failed = FALSE;
  if (substr($resp, 8, 8) == 'DECLINED') {
    $result_code = 'DECLINED';
    $failed = TRUE;
  }
  elseif (substr($resp, 8, 6) == 'FAILED') {
    $result_code = 'FAILED: ' . $result_code;
    $failed = TRUE;
  }

  if (strstr($resp, 'RESULT:')) {

    // $resp example RESULT: OK RESULT_CODE: 000 3DSECURE: NOTPARTICIPATED
    // RRN: 915300393049 APPROVAL_CODE: 705368 CARD_NUMBER: 4***********9913.
    if (strstr($resp, 'RESULT:')) {
      $result = explode('RESULT: ', $resp);
      $result = preg_split('/\r\n|\r|\n/', $result[1]);
      $result = $result[0];
    }
    else {
      $result = '';
    }

    if (strstr($resp, 'RESULT_CODE:')) {
      $result_code = explode('RESULT_CODE: ', $resp);
      $result_code = preg_split('/\r\n|\r|\n/', $result_code[1]);
      $result_code = $result_code[0];
    }
    else {
      $result_code = '';
    }

    if (strstr($resp, '3DSECURE:')) {
      $result_3dsecure = explode('3DSECURE: ', $resp);
      $result_3dsecure = preg_split('/\r\n|\r|\n/', $result_3dsecure[1]);
      $result_3dsecure = $result_3dsecure[0];
    }
    else {
      $result_3dsecure = '';
    }

    if (strstr($resp, 'CARD_NUMBER:')) {
      $card_number = explode('CARD_NUMBER: ', $resp);
      $card_number = preg_split('/\r\n|\r|\n/', $card_number[1]);
      $card_number = $card_number[0];
    }
    else {
      $card_number = '';
    }

    $result = db_update('commerce_ibis_transaction')
    ->fields(array(
      'order_id' => $order_id,
      'result' => $result,
      'result_code' => $result_code,
      'result_3dsecure' => $result_3dsecure,
      'card_number' => $card_number,
      'response' => $resp,
    ))
    ->condition('trans_id', $trans_id)
    ->execute();
  }
  else {
    $result = db_insert('commerce_ibis_error')
    ->fields(array(
      'error_time' => $now,
      'action' => 'ReturnOkURL',
      'response' => $resp,
    ))
    ->execute();
  }

  $transaction = commerce_payment_transaction_new('ibis', $order_id);

  if ($failed == TRUE) {
    global $user;

    // Save POST information.
    $post_ser = serialize($_REQUEST);
    // Save responce.
    $responce_ser = serialize($resp);
    drupal_set_message(t('Payment failed. Check card number and expiration date.'));

    $transaction->message = t('IBIS payment failed. Check card data and try again @resp.');
    $transaction->message_variables = array('@resp' => $responce_ser);

    watchdog('ibis', 'Payment failed: !result', array('!result' => $resp));

    $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
  }
  else {
    $order = commerce_order_load($order_id);

    watchdog('ibis', 'Receiving new order notification for order !order_id.', array('!order_id' => $order->order_id));
    watchdog('ibis', 'Payment received: !result', array('!result' => $resp));

    $wrapper = entity_metadata_wrapper('commerce_order', $order_id);
    $currency = $wrapper->commerce_order_total->currency_code->value();
    $amount = $wrapper->commerce_order_total->amount->value();

    $transaction->amount = $amount;
    $transaction->currency_code = $currency;
    $transaction->remote_status = t('Success');
    $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
    $transaction->message = t('Order #!order payment', array('!order' => check_plain($order->order_number)));
    $transaction->message_variables = array('@date' => date("d-m-Y H:i:s", REQUEST_TIME));
  }

  $transaction->instance_id = 'ibis';

  commerce_payment_transaction_save($transaction);

  drupal_set_message(t('Thank you for your payment!'));
  drupal_goto();
  return TRUE;
}

/**
 * Payment failed function.
 */
function commerce_ibis_failed() {
  $settings = commerce_ibis_get_rule_settings();

  $ecomm_server_url = ($settings['commerce_ibis_server']['commerce_ibis_server'] == 'test') ? $settings['commerce_ibis_server']['server_url_test'] : $settings['commerce_ibis_server']['server_url_live'];

  $trans_id = $_REQUEST['trans_id'];
  $error_msg = $_REQUEST['error'];
  $now = REQUEST_TIME;

  $result = db_query("SELECT client_ip_addr FROM {commerce_ibis_transaction} WHERE `trans_id` = :`trans_id`", array(':`trans_id`' => $trans_id));

  $client_ip_addr = $result->fetchField('client_ip_addr');

  $merchant = new Merchant($ecomm_server_url, $settings['commerce_ibis_server']['commerce_ibis_cert_path'], $settings['commerce_ibis_server']['commerce_ibis_cert_pass'], 1);

  $resp = $merchant->getTransResult(urlencode($trans_id), $client_ip_addr);
  $resp = $error_msg . ' + ' . $resp;
  $result = db_insert('commerce_ibis_error')
  ->fields(array(
    'error_time' => $now,
    'action' => 'ReturnFailURL',
    'response' => $resp,
  ))
  ->execute();
  drupal_set_message(check_plain(t('Technical error occurred! Please contact merchant! !message'), array('!message' => $error_msg)));
  drupal_goto();
}

/**
 * Reverse payment page.
 */
function commerce_ibis_reverse_page() {
  $output = drupal_get_form('commerce_ibis_reverse_form');

  return $output;
}

/**
 * Reverse payment form.
 */
function commerce_ibis_reverse_form($form, $form_state) {
  global $language;

  $order_id = arg(3);

  $wrapper = entity_metadata_wrapper('commerce_order', $order_id);
  $currency = $wrapper->commerce_order_total->currency_code->value();
  $amount = $wrapper->commerce_order_total->amount->value();

  $result = db_query("SELECT trans_id FROM {commerce_ibis_transaction} WHERE order_id = :order_id", array(':order_id' => $order_id));
  $trans_id = $result->fetchField();

  $data = array(
    'order_id' => $order_id,
    'lang' => $language->language,
  );

  $form['#action'] = base_path() . 'cart/ibis/reverse';

  foreach ($data as $name => $value) {
    $form[$name] = array(
      '#type' => 'hidden',
      '#value' => $value,
    );
  }
  $form['trans_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Transaction ID'),
    '#value' => $trans_id,
  );
  $form['amount'] = array(
    '#type' => 'textfield',
    '#title' => check_plain(t('Amount')),
    '#value' => $amount,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Reverse payment'),
  );

  return $form;
}

/**
 * Reverse payment to customer.
 */
function commerce_ibis_reverse() {
  $settings = commerce_ibis_get_rule_settings();

  $ecomm_server_url = ($settings['commerce_ibis_server']['commerce_ibis_server'] == 'test') ? $settings['commerce_ibis_server']['server_url_test'] : $settings['commerce_ibis_server']['server_url_live'];

  $id = $_REQUEST['order_id'];
  $trans_id = urlencode($_REQUEST['trans_id']);
  $amount = $_REQUEST['amount'] * 100;
  $now = REQUEST_TIME;

  if ($amount == '0') {
    drupal_set_message(t('Amount invalid. <a href=\"javascript:history.go(-1)\"><< Back </a>'));
  }
  $merchant = new Merchant($ecomm_server_url, $settings['commerce_ibis_server']['commerce_ibis_cert_path'], $settings['commerce_ibis_server']['commerce_ibis_cert_pass'], 1);
  $resp = $merchant->reverse($trans_id, $amount);

  if (substr($resp, 8, 2) == "OK" || substr($resp, 8, 8) == "REVERSED") {

    if (strstr($resp, 'RESULT:')) {
      $result = explode('RESULT: ', $resp);
      $result = preg_split('/\r\n|\r|\n/', $result[1]);
      $result = $result[0];
    }
    else {
      $result = '';
    }

    if (strstr($resp, 'RESULT_CODE:')) {
      $result_code = explode('RESULT_CODE: ', $resp);
      $result_code = preg_split('/\r\n|\r|\n/', $result_code[1]);
      $result_code = $result_code[0];
    }
    else {
      $result_code = '';
    }

    $result = db_update('commerce_ibis_transaction')
    ->fields(array(
        'reversal_amount' => $amount,
        'result_code' => $result_code,
        'result' => $result,
        'response' => $resp,
      ))
    ->condition('trans_id', $_REQUEST['trans_id'])
    ->execute();
    watchdog('ibis', 'Payment reversed: !result', array('!result' => $resp));
    drupal_set_message(t('Payment reversed.'));
    drupal_goto('admin/store/orders/' . $id);

  }
  else {
    watchdog('ibis', 'Payment reverse failed: !result', array('!result' => $resp . $trans_id));
    drupal_set_message(t('Payment reverse failed.'));
    drupal_goto('admin/store/orders/' . $id);
    $result = db_insert('commerce_ibis_error')
    ->fields(array(
      'error_time' => $now,
      'action' => 'reverse',
      'response' => $resp,
    ))
    ->execute();
  }
}
